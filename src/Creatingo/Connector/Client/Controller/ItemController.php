<?php

/**
 * This file is part of the Creatingo Stock Image connector.
 *
 * @package    cloud-connector
 * @author     David Molineus <david.molineus@netzmacht.de>
 * @copyright  2014 netzmacht creative David Molineus
 * @license    CC BY-NC-ND 4.0
 *
 */

namespace Creatingo\Connector\Client\Controller;


use Creatingo\Connector\Client\Api\Items;
use Creatingo\Connector\Client\Installation\Installation;
use Creatingo\Connector\Client\Response\Notification;
use Creatingo\Connector\Client\Response\Template;


class ItemController
{
	/**
	 * @var Items
	 */
	private $items;

	/**
	 * @var Installation
	 */
	private $installation;


	/**
	 * @param Items $items
	 * @param \Creatingo\Connector\Client\Installation\Installation $installation
	 */
	function __construct(Items $items, Installation $installation)
	{
		$this->items = $items;
		$this->installation = $installation;
	}


	/**
	 * @param $category
	 * @param $tag
	 * @param $color
	 * @param null $type
	 * @param int $page
	 * @return \Creatingo\Connector\Client\Response\Template
	 */
	public function listItems($category, $tag, $color, $type=null, $page=1)
	{
		$response = $this->items->find($category, $color, $tag, $type, $page);
		$response = array_merge($response, array(
			'categories'   => $this->items->listCategories(),
			'colors'	   => $this->items->listColors(),
			'installed'    => $this->installation->getInstalledItems(),
		));

		return new Template('creatingo_list', $response);
	}


	/**
	 * @param $id
	 * @return \Creatingo\Connector\Client\Response\Notification
	 */
	public function downloadItem($id )
	{
		$item = $this->items->details($id);
		$file = $this->items->download($id);

		$this->installation->installItem($item, $file);
		$success = true;
		$type    = $success ? Notification::SUCCESS : Notification::ERROR;
		$message = 'item.download.' . ($success ? 'success' : 'error');
		$data    = array($item->getName());

		return new Notification($type, $message, $data);
	}

} 
