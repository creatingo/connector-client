<?php

/**
 * This file is part of the Creatingo Stock Image connector.
 *
 * @package    cloud-connector
 * @author     David Molineus <david.molineus@netzmacht.de>
 * @copyright  2014 netzmacht creative David Molineus
 * @license    CC BY-NC-ND 4.0
 *
 */

namespace Creatingo\Connector\Client\Domain;


class Item
{
	const THUMB_SMALL  = 'small';
	const THUMB_MEDIUM = 'medium';

	/**
	 * @var
	 */
	private $id;

	/**
	 * @var
	 */
	private $file;

	/**
	 * @var
	 */
	private $description;

	/**
	 * @var string
	 */
	private $category;

	/**
	 * @var array
	 */
	private $tags = array();

	/**
	 * @var array
	 */
	private $colors = array();

	/**
	 * @var string
	 */
	private $name;

	/**
	 * @var string
	 */
	private $size;

	/**
	 * @var string
	 */
	private $fileType;

	/**
	 * @var string[]
	 */
	private $thumbnails;

	/**
	 * @var string
	 */
	private $purchaseUrl;

	/**
	 * @var string
	 */
	private $purchaseLabel;

	/**
	 * @var bool
	 */
	private $available;



	/**
	 * @return array
	 */
	public function toArray()
	{
		return get_object_vars($this);
	}


	/**
	 * @param array $data
	 * @return Item
	 */
	public static function fromArray(array $data)
	{
		$item = new static;

		// dynamically set other attributes
		foreach($data as $name => $value) {
			$item->$name = $value;
		}

		return $item;
	}


	/**
	 * @return string
	 */
	public function getFile()
	{
		return $this->file;
	}


	/**
	 * @param $category
	 * @return bool
	 */
	public function isCategorizedIn($category)
	{
		return $this->category == $category;
	}


	/**
	 * @param $keyword
	 * @return bool
	 */
	public function hasTag($keyword)
	{
		return in_array($keyword, $this->tags);
	}


	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}


	/**
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param bool $label
	 * @return string
	 */
	public function getCategory($label=false)
	{
		if($label) {
			return $this->category_label;
		}

		return $this->category;
	}

	/**
	 * @return array
	 */
	public function getColors()
	{
		return $this->colors;
	}

	/**
	 * @return array
	 */
	public function getTags()
	{
		return $this->tags;
	}


	/**
	 * @return string
	 */
	public function getSize()
	{
		return $this->size;
	}


	/**
	 * @return mixed
	 */
	public function getFileType()
	{
		return $this->fileType;
	}


	/**
	 * @param $size
	 * @return \string[]
	 */
	public function getThumbnail($size=Item::THUMB_SMALL)
	{
		if(isset($this->thumbnails[$size])) {
			return $this->thumbnails[$size];
		}

		return null;
	}


	/**
	 * @return string
	 */
	public function getPurchaseLabel()
	{
		return $this->purchaseLabel;
	}

	/**
	 * @return string
	 */
	public function getPurchaseUrl()
	{
		return $this->purchaseUrl;
	}


	/**
	 * @return string
	 */
	public function isAvailable()
	{
		return $this->available;
	}

}
