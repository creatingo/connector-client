<?php

/**
 * This file is part of the Creatingo Stock Image connector.
 *
 * @package    cloud-connector
 * @author     David Molineus <david.molineus@netzmacht.de>
 * @copyright  2014 netzmacht creative David Molineus
 * @license    CC BY-NC-ND 4.0
 *
 */

namespace Creatingo\Connector\Client\Domain;


class Account
{
	const TYPE_FREE = 'free';

	const TYPE_PREMIUM  = 'premium';


	/**
	 * @var string
	 */
	private $type;

	/**
	 * @var string
	 */
	private $token;

	/**
	 * @var int
	 */
	private $id;

	/**
	 * @var string
	 */
	private $username;

	/**
	 * @var \DateTime
	 */
	private $premiumUntil;


	/**
	 * @param $id
	 * @param $username
	 * @param $type
	 * @param $token
	 * @param \DateTime $premiumUntil
	 */
	function __construct($id, $username, $type, $token, \DateTime $premiumUntil = null)
	{
		$this->id       = $id;
		$this->type     = $type;
		$this->token    = $token;
		$this->username = $username;

		if($this->isTypeOf(static::TYPE_PREMIUM)) {
			$this->premiumUntil = $premiumUntil;
		}
	}


	/**
	 * @param array $array
	 * @return Account
	 */
	public static function fromArray(array $array)
	{
		$premiumUntil = null;

		if($array['premiumUntil']) {
			$premiumUntil = new \DateTime();
			$premiumUntil->setTimestamp($array['premiumUntil']);
		}

		$account = new Account(
			$array['id'],
			$array['username'],
			$array['type'],
			$array['token'],
			$premiumUntil
		);

		return $account;
	}

	/**
	 * @return array
	 */
	public function toArray()
	{
		$data = get_object_vars($this);

		if($this->getPremiumUntil()) {
			$data['premiumUntil'] = $this->getPremiumUntil()->getTimestamp();
		}

		return $data;
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getToken()
	{
		return $this->token;
	}

	/**
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}


	/**
	 * @param $type
	 * @return bool
	 */
	public function isTypeOf($type)
	{
		return $this->type === $type;
	}


	/**
	 * @return string
	 */
	public function getUsername()
	{
		return $this->username;
	}

	/**
	 * @return \DateTime
	 */
	public function getPremiumUntil()
	{
		return $this->premiumUntil;
	}

} 
