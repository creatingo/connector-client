<?php

/**
 * This file is part of the Creatingo Stock Image connector.
 *
 * @package    cloud-connector
 * @author     David Molineus <david.molineus@netzmacht.de>
 * @copyright  2014 netzmacht creative David Molineus
 * @license    CC BY-NC-ND 4.0
 *
 */

namespace Creatingo\Connector\Client\Installation;


class InstalledItem
{
	/**
	 * @var
	 */
	private $itemId;

	/**
	 * @var array
	 */
	private $data;

	/**
	 * @param $itemId
	 * @param array $data
	 */
	function __construct($itemId, array $data)
	{
		$this->itemId = $itemId;
		$this->data   = $data;
	}

	/**
	 * @return array
	 */
	public function getData()
	{
		return $this->data;
	}


	/**
	 * @return mixed
	 */
	public function getItemId()
	{
		return $this->itemId;
	}


	/**
	 * @param $key
	 * @return null
	 */
	public function get($key)
	{
		if(isset($this->data[$key])) {
			return $this->data[$key];
		}

		return null;
	}

} 
