<?php

/**
 * This file is part of the Creatingo Stock Image connector.
 *
 * @package    cloud-connector
 * @author     David Molineus <david.molineus@netzmacht.de>
 * @copyright  2014 netzmacht creative David Molineus
 * @license    CC BY-NC-ND 4.0
 *
 */

namespace Creatingo\Connector\Client\Installation;


use Creatingo\Connector\Client\Domain\Item;
use Traversable;

class InstalledItemCollection implements \IteratorAggregate
{
	/**
	 * @var InstalledItem[]
	 */
	private $items;


	/**
	 * @param InstalledItem[] $items ids of items
	 */
	function __construct(array $items=array())
	{
		$this->items = $items;
	}


	/**
	 * @return InstalledItem[]
	 */
	public function getItems()
	{
		return $this->items;
	}


	/**
	 * @param InstalledItem $item
	 * @return $this
	 */
	public function addItem(InstalledItem $item)
	{
		$this->items[] = $item;

		return $this;
	}


	/**
	 * @param Item|int $itemId
	 * @return bool
	 */
	public function isInstalled($itemId)
	{
		if($itemId instanceof Item) {
			$itemId = $itemId->getId();
		}
		elseif($itemId instanceof InstalledItem) {
			$itemId = $itemId->getItemId();
		}

		foreach($this->items as $installed) {
			if($installed->getItemId() == $itemId) {
				return true;
			}
		}

		return false;
	}


	/**
	 * @param $itemId
	 * @throws \InvalidArgumentException
	 * @return InstalledItem
	 */
	public function getInstalledItem($itemId)
	{
		if($itemId instanceof Item) {
			$itemId = $itemId->getId();
		}

		foreach($this->items as $installed) {
			if($installed->getItemId() == $itemId) {
				return $installed;
			}
		}

		throw new \InvalidArgumentException(sprintf('Item "%s" not found', $itemId));
	}


	/**
	 * (PHP 5 &gt;= 5.0.0)<br/>
	 * Retrieve an external iterator
	 * @link http://php.net/manual/en/iteratoraggregate.getiterator.php
	 * @return Traversable An instance of an object implementing <b>Iterator</b> or
	 * <b>Traversable</b>
	 */
	public function getIterator()
	{
		return new \ArrayIterator($this->items);
	}

}
