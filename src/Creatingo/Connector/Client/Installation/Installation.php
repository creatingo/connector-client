<?php

/**
 * This file is part of the Creatingo Stock Image connector.
 *
 * @package    cloud-connector
 * @author     David Molineus <david.molineus@netzmacht.de>
 * @copyright  2014 netzmacht creative David Molineus
 * @license    CC BY-NC-ND 4.0
 *
 */

namespace Creatingo\Connector\Client\Installation;


use Creatingo\Connector\Client\Domain\Item;


/**
 * Interface Filesystem
 * @package Creatingo\Connector\Client\Filesystem
 */
interface Installation
{

	/**
	 * @param Item $item
	 * @param $file
	 * @param bool $force
	 * @return void
	 */
	public function installItem(Item $item, $file, $force=false);


	/**
	 * @return InstalledItemCollection
	 */
	public function getInstalledItems();

} 
