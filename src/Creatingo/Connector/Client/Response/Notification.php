<?php

/**
 * This file is part of the Creatingo Stock Image connector.
 *
 * @package    cloud-connector
 * @author     David Molineus <david.molineus@netzmacht.de>
 * @copyright  2014 netzmacht creative David Molineus
 * @license    CC BY-NC-ND 4.0
 *
 */

namespace Creatingo\Connector\Client\Response;


class Notification
{
	const INFO = 'info';
	const ERROR = 'error';
	const SUCCESS = 'success';

	/**
	 * @var string
	 */
	private $message;

	/**
	 * @var string
	 */
	private $type;

	/**
	 * @var array
	 */
	private $data;


	/**
	 * @param $type
	 * @param $message
	 * @param array $data
	 */
	function __construct($type, $message, array $data=array())
	{
		$this->type    = $type;
		$this->message = $message;
		$this->data    = $data;
	}

	/**
	 * @return string
	 */
	public function getMessage()
	{
		return $this->message;
	}

	/**
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * @return array
	 */
	public function getData()
	{
		return $this->data;
	}

} 
