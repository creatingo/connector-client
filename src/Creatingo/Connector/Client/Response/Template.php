<?php

/**
 * This file is part of the Creatingo Stock Image connector.
 *
 * @package    cloud-connector
 * @author     David Molineus <david.molineus@netzmacht.de>
 * @copyright  2014 netzmacht creative David Molineus
 * @license    CC BY-NC-ND 4.0
 *
 */

namespace Creatingo\Connector\Client\Response;


class Template
{
	/**
	 * @var string
	 */
	private $template;

	/**
	 * @var array
	 */
	private $data;

	/**
	 * @var string
	 */
	private $contentType;

	/**
	 * @param string $template name of themplate
	 * @param array $data
	 * @param string $contentType
	 */
	function __construct($template, array $data=array(), $contentType = 'text/html')
	{
		$this->template    = $template;
		$this->data        = $data;
		$this->contentType = $contentType;
	}

	/**
	 * @param string $contentType
	 */
	public function setContentType($contentType)
	{
		$this->contentType = $contentType;
	}

	/**
	 * @return string
	 */
	public function getContentType()
	{
		return $this->contentType;
	}

	/**
	 * @param array $data
	 */
	public function setData($data)
	{
		$this->data = $data;
	}

	/**
	 * @return array
	 */
	public function getData()
	{
		return $this->data;
	}

	/**
	 * @param string $template
	 */
	public function setTemplate($template)
	{
		$this->template = $template;
	}

	/**
	 * @return string
	 */
	public function getTemplate()
	{
		return $this->template;
	}


	/**
	 * @param $data
	 *
	 * @return $this
	 */
	public function addData(array $data)
	{
		$this->data = array_merge($this->data, $data);

		return $this;
	}

} 
