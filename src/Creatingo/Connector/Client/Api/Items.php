<?php

/**
 * This file is part of the Creatingo Stock Image connector.
 *
 * @package    cloud-connector
 * @author     David Molineus <david.molineus@netzmacht.de>
 * @copyright  2014 netzmacht creative David Molineus
 * @license    CC BY-NC-ND 4.0
 *
 */

namespace Creatingo\Connector\Client\Api;

use Creatingo\Connector\Client\Domain\Item;
use Guzzle\Http\Client as Guzzle;

class Items
{
	const TYPE_ALL = 'all';
	const TYPE_PREMIUM = 'premium';
	const TYPE_FREE = 'free';

	/**
	 * @var Guzzle
	 */
	private $httpClient;


	/**
	 * @param $httpClient
	 */
	function __construct($httpClient)
	{
		$this->httpClient = $httpClient;
	}


	/**
	 * @param string $category
	 * @param string $color
	 * @param string $keyword
	 * @param string $type
	 * @param int $page
	 * @return array
	 */
	public function find($category=null, $color=null, $keyword=null, $type=Items::TYPE_ALL, $page=1)
	{
		$request = $this->httpClient->get('items');

		if(!$type) {
			$type = Items::TYPE_ALL;
		}

		$query = $request->getQuery()
			->set('page', $page)
			->set('type', $type);

		$keyword  && $query->set('keyword', $keyword);
		$category && $query->set('category', $category);
		$color    && $query->set('color', $color);

		$response = $request->send();
		Client::guardValidResponse($response);

		$data  = $response->json();
		$items = array();

		foreach($data['items'] as $item) {
			$items[] = Item::fromArray($item);
		}

		$data['items'] = $items;

		return $data;
	}


	/**
	 * @param $id
	 * @return Item
	 */
	public function details($id)
	{
		$response = $this->httpClient
			->get(sprintf('items/%s', $id))
			->send();

		Client::guardValidResponse($response);

		$data = $response->json();
		$item = Item::fromArray($data);

		return $item;
	}


	/**
	 * @param $id
	 * @return resource
	 */
	public function download($id)
	{
		$response = $this->httpClient
			->get(sprintf('items/%s/download', $id))
			->send();

		Client::guardValidResponse($response);

		return $response->getBody(true);
	}


	/**
	 * @return array
	 */
	public function listColors()
	{
		$response = $this->httpClient
			->get('colors')
			->send();

		Client::guardValidResponse($response);

		return $response->json();
	}


	/**
	 * @return array
	 */
	public function listCategories()
	{
		$response = $this->httpClient
			->get('categories')
			->send();

		Client::guardValidResponse($response);

		return $response->json();
	}

} 
