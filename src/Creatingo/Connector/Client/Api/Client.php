<?php

/**
 * This file is part of the Creatingo Stock Image connector.
 *
 * @package    cloud-connector
 * @author     David Molineus <david.molineus@netzmacht.de>
 * @copyright  2014 netzmacht creative David Molineus
 * @license    CC BY-NC-ND 4.0
 *
 */

namespace Creatingo\Connector\Client\Api;

use Creatingo\Connector\Client\Domain\Account;
use Guzzle\Http\Client as Guzzle;
use Guzzle\Http\Message\Response;

class Client
{
	/**
	 * @var Guzzle
	 */
	private $httpClient;


	/**
	 * @param Guzzle $httpClient
	 */
	function __construct(Guzzle $httpClient)
	{
		$this->httpClient = $httpClient;
	}


	/**
	 * @param $serverUri
	 * @param $authToken
	 * @param array $headers
	 * @return Client
	 */
	public static function connect($serverUri, $authToken, array $headers=array())
	{
		$httpClient = new Guzzle($serverUri);
		$httpClient->setDefaultOption('auth', array($authToken, null, 'Basic'));

		$httpClient->setDefaultOption('headers',
			array_merge(
				$headers,
				array('Accept' => 'application/json', 'Content-Type' => 'application/json')
			)
		);

		return new static($httpClient);
	}


	/**
	 * @return \Guzzle\Http\Client
	 */
	public function getHttpClient()
	{
		return $this->httpClient;
	}


	/**
	 * @return Account
	 */
	public function account()
	{
		$response = $this->httpClient
			->get('account')
			->send();

		$this->guardValidResponse($response);

		$data    = $response->json();
		$account = Account::fromArray($data);

		return $account;
	}


	/**
	 * @return Items
	 */
	public function items()
	{
		return new Items($this->httpClient);
	}


	/**
	 * @param Response $response
	 * @throws \HttpResponseException
	 */
	public static function guardValidResponse(Response $response)
	{
		switch($response->getStatusCode()) {
			case 200:
				break;

			case 500:
				throw new \HttpResponseException('Server error');
				break;

			default:
				if(!$response->getBody()->isReadable()) {
					throw new \HttpResponseException('Response body is not readable');
				}

				throw new \HttpResponseException($response->getBody(true));
		}
	}

}
